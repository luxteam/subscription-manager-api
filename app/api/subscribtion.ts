import {NextFunction, Request, Response, Router} from "express";
import {BaseRoute} from "../routes/route";
import {NodeCache} from "node-cache";
import {HttpError} from "http-errors";

import * as EmailValidator from 'email-validator';
import {IsValidEmail} from "../models/is-valid-email.model";
import {UnsubscribeResponse} from "../models/unsubsribe-response";

export class SubscriptionRoute extends BaseRoute {

    private static routeName = 'subscription';

    /**
     * Constructor
     *
     * @class SubscriptionRoute
     * @constructor
     * @param cache {NodeCache} The node-cache object.
     */
    constructor(cache: NodeCache) {
        super(cache);
    }

    /**
     * Creates the routes.
     *
     * @class SubscriptionRoute
     * @method create
     * @static
     * @param router {Router} The express router.
     * @param cache {NodeCache} The node-cache object.
     */
    public static create(router: Router, cache: NodeCache) {
        console.log("[SubscriptionRoute::create] Creating subscription route.");

        router.get(`/${this.routeName}/amount`, (req: Request, res: Response, next: NextFunction) => {
            new SubscriptionRoute(cache).getAmount(req, res, next);
        });

        router.get(`/${this.routeName}`, (req: Request, res: Response, next: NextFunction) => {
            new SubscriptionRoute(cache).getByQuery(req, res, next);
        });

        router.post(`/${this.routeName}`, (req: Request, res: Response, next: NextFunction) => {
            new SubscriptionRoute(cache).subscribe(req, res, next);
        });

        router.post(`/${this.routeName}/unsubscribe`, (req: Request, res: Response, next: NextFunction) => {
            new SubscriptionRoute(cache).unsubscribe(req, res, next);
        });
    }

    /**
     * Gets email from cache storage by query.
     *
     * @class SubscriptionRoute
     * @method getByQuery
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} Execute the next method.
     */
    public getByQuery(req: Request, res: Response, next: NextFunction): void {
        const responseBody = this.checkEmail(req.query.email);
        this.sendResponse(responseBody, res, next);
    }

    /**
     * Gets amount of subscriptions from the cache storage.
     *
     * @class SubscriptionRoute
     * @method getByParams
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} Execute the next method.
     */
    public getAmount(req: Request, res: Response, next: NextFunction): void {
        const amount = this.totalSubscription;
        res.send({amount});
    }

    /**
     * Email unsubscribe.
     *
     * @class SubscriptionRoute
     * @method unsubscribe
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} Execute the next method.
     */
    public unsubscribe(req: Request, res: Response, next: NextFunction): void {
        const isExist = this.cacheItems.get(req.body.email);
        if (EmailValidator.validate(req.body.email) && isExist) {
            this.cacheItems.del(req.body.email);
        } else {
            const error: HttpError = {
                expose: true,
                message: 'this email is in a wrong format or not in subscribed list',
                name: 'HttpError',
                status: 405,
                statusCode: 405
            };
            return next(error)
        }

        const response: UnsubscribeResponse = {
            success: `the email ${req.body.email} is unsubscribed`,
            totalSubscriptions: this.totalSubscription
        };
        res.send(response);
    }

    /**
     * Email subscribtion.
     *
     * @class SubscriptionRoute
     * @method subscribe
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} Execute the next method.
     */
    public subscribe(req: Request, res: Response, next: NextFunction): void {
        console.log('subscribe!!!');
        console.log(req);
        const isExist = this.cacheItems.get(req.body.email);
        let value: boolean;

        if (EmailValidator.validate(req.body.email) && !isExist) {
            value = this.cacheItems.set(req.body.email, req.body.email);
        } else {
            const error: HttpError = {
                expose: true,
                message: 'email is in a wrong format or already set',
                name: 'HttpError',
                status: 208,
                statusCode: 208
            };
            return next(error)
        }

        const responseBody = this.populateEmailResponse(value, req.body.email);
        res.send(responseBody);
    }

    /**
     * Checks if an email in the storage.
     *
     * @class SubscriptionRoute
     * @method checkEmail
     * @param email {string} An email to be checked.
     * @return {IsValidEmail} IsValidEmail model
     */
    private checkEmail(email: string): IsValidEmail {
        const value = this.cacheItems.get(email);
        return this.populateEmailResponse(value, email);
    }

    /**
     * Sends response.
     *
     * @class SubscriptionRoute
     * @method sendResponse
     * @param responseBody {IsValidEmail} A response object.
     * @param res {Response} The express Response object.
     * @param next {NextFunction} Execute the next method.
     */
    private sendResponse(responseBody: IsValidEmail, res: Response, next: NextFunction): void {
        responseBody.totalSubscriptions = this.totalSubscription;
        if (responseBody.isValid) {
            res.send(responseBody);
        } else {
            const error: HttpError = {
                expose: true,
                message: 'email is not valid',
                name: 'HttpError',
                status: 406,
                statusCode: 406
            };
            return next(error)
        }
    }

    /**
     * Populates response.
     *
     * @class SubscriptionRoute
     * @method populateEmailResponse
     * @param value {any} A result of the node-cache storage check.
     * @param email {string} An email to be in response.
     * @param next {NextFunction} Execute the next method.
     */
    private populateEmailResponse(value: any, email: string): IsValidEmail {
        const responseBody = new IsValidEmail();
        if (EmailValidator.validate(email) && value) {
            responseBody.email = email;
            responseBody.isValid = true;
        } else {
            responseBody.email = null;
            responseBody.isValid = false;
        }

        responseBody.totalSubscriptions = this.totalSubscription;
        return responseBody;
    }
}