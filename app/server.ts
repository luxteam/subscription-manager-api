import * as bodyParser from "body-parser";
import * as express from "express";
import * as createError from "http-errors";
import {HttpError} from "http-errors";

import {SubscriptionRoute} from "./api/subscribtion";
import NodeCache = require('node-cache');

export class Server {
    public app: express.Application;

    private readonly router: express.Router;
    private readonly nodeCache: NodeCache;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {
        this.app = express();
        this.router = express.Router();
        this.nodeCache = new NodeCache();

        this.config();
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    private config(): void {

        this.app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));

        this.routes();

        this.app.use(this.router);
        this.app.use((req, res, next) => {
            next(createError(404));
        });
        this.app.use((err: HttpError, req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};

            res.status(err.status || 500);
            res.send(err.message)
        });
    }

    /**
     * Create router
     *
     * @class Server
     * @method routes
     */
    public routes(): void {
        SubscriptionRoute.create(this.router, this.nodeCache);
    }
}