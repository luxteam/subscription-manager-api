export class UnsubscribeResponse {
    success: string;
    totalSubscriptions: number;
}