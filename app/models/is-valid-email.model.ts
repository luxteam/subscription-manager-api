export class IsValidEmail {
    totalSubscriptions: number;
    isValid: boolean;
    email: string;
}