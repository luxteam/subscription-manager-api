import {NodeCache} from "node-cache";

export class BaseRoute {
    private readonly _cacheItems: NodeCache;

    /**
     * Get emails total count.
     *
     * @class BaseRoute
     * @property totalSubscription
     * @return {number} total count
     */
    public get totalSubscription(): number {
        return this._cacheItems.getStats().keys;
    }

    public get cacheItems(): NodeCache {
        return this._cacheItems;
    }

    /**
     * Constructor
     *
     * @class BaseRoute
     * @constructor
     * @param cache {NodeCache} The node-cache object.
     */
    constructor(cache: NodeCache) {
        this._cacheItems = cache;
    }
}